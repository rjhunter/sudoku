#!/usr/bin/env python
from __future__ import division, print_function
import sys
from itertools import combinations
import pycosat

def reduce_it(sudoku):
    """ Input: a 9x9 sudoku puzzle represented as an 81-char string.
    Valid values are 0-9, where 0 indicates an unsolved square, and
    1-9 are hints.
    """

    assert len(sudoku) == 81
    numset = set(sudoku)
    assert numset.issubset(set("0123456789"))

    board = []

    it = iter(sudoku)
    for i in xrange(9):
        row = []
        for j in xrange(9):
            row.append(int(it.next()))
        board.append(row)

    def encode(num, row, col):
        return 9 * ((9 * row) + col) + num + 1

    def decode(prop):
        prop -= 1
        num = prop % 9
        col = (prop // 9) % 9
        row = (prop // 9) // 9
        return num, row, col

    def decode_soln(soln):
        nums = [decode(n)[0] + 1 for n in soln if n > 0]
        assert len(nums) == 81
        return ''.join(["%d" % n for n in nums])
            
    clauses = []

    # every square has at least one number
    for i in xrange(9):
        for j in xrange(9):
            clauses.append([encode(n, i, j) for n in xrange(9)])
        
    # sudoku constraints
    for i in xrange(9):
        for j in xrange(9):
            for m, n in combinations(xrange(9), 2):
                # every square has at most one number
                clauses.append([-encode(m, i, j), -encode(n, i, j)])
                # every column has at most one of each number
                clauses.append([-encode(i, m, j), -encode(i, n, j)])
                # every row has at most one of each number
                clauses.append([-encode(i, j, m), -encode(i, j, n)])
                # every quadrant has at most one of each number
                clauses.append([-encode(i, 3 * (j // 3) + (m // 3), 3 * (j % 3) + m % 3),
                                 -encode(i, 3 * (j // 3) + (n // 3), 3 * (j % 3) + n % 3)])

    # given board
    for i in xrange(9):
        for j in xrange(9):
            if board[i][j] != 0:
                clauses.append([encode(board[i][j] - 1, i, j)])

    #print(clauses)
    return clauses, decode_soln



 
##End Your Code HERE...


def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in xrange(0, len(l), n):
        yield l[i:i+n]

def print_board(board):
    board = board.replace("0"," ")
    for tier in chunks(board, 27):
        for row in chunks(tier, 9):
            print('|'.join('{}{}{}'.format(*quad) for quad in chunks(row, 3)))
        print('-' * 11)

def solve(sudoku):
    print_board(sudoku)

    print("    |")
    print("    |")
    print("    |")
    print("    V")

    clauses, decode_soln  = reduce_it(sudoku)

    if pycosat.solve(clauses) == 'UNSAT':
        print('No solution found')
    else:
        for soln in pycosat.itersolve(clauses):
            print_board(decode_soln(soln))
            print("\n\n")
            break
def main():
    sudokus = ["050602010800000003000000000900876005000000000400259001000000000700000006020708090",
               "000000000604010203019000740040000020000602000020000070082000530407090801000000000",
               "050602010800000003000000000900876005000000000400259001000000000700000006020708090",
               "000000000604010203019000740040000020000602000020000070082000530407090801000000000",
               "000080000001706800060502030047000310100000006085000720030901040009803200000050000",
               "000210070300040500027000004061000009000050000800000760400000830009070001050084000",
               "570000008009200004000080010000000080001020700060000000030090000600004900900000056",
               ]

    for sudoku in sudokus:
        solve(sudoku)
                    


if __name__ == "__main__":
    main()
